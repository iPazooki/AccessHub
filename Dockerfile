# Create a container for runtime
FROM microsoft/dotnet:2.1.5-aspnetcore-runtime
WORKDIR /app
COPY ./publish/ .
ENTRYPOINT ["dotnet", "website.dll"]