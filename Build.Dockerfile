FROM geeksbasebuild:latest as build
ARG BUILD_NUMBER=1.0
ARG HUB_SERVICE_URL=http://localhost:9011
ARG BUILD_MODE=debug

RUN build-project $BUILD_MODE

FROM microsoft/dotnet:2.1.5-aspnetcore-runtime
WORKDIR app 
COPY --from=build app/output/ .
ENTRYPOINT ["dotnet","website.dll"]