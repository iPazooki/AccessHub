using Domain;
using MSharp;

namespace Modules
{
    public class BoardView : ViewModule<Domain.Board>
    {
        public BoardView()
        {
            IsViewComponent();

            ViewModelProperty<string>("FeatureId").FromRequestParam("featureId");

            WrapInForm(value: false);

            Markup(@"<div data-module='BoardView'>
     @Html.StartupActionsJson()
     <div class='gridster'>
         <ul>
             @{ int row = 1, col = 0; }
             @foreach (var widget in item.GetWidgets(User))
             {
                 col++;
                 <li data-row='@row' data-col='@col' data-sizex='1' data-sizey='1'>
                     @widget.Render(info.FeatureId).Raw()
                 </li>
                 if (col == 4) { col = 0; row++; }
             }
         </ul>
     </div>
 </div>").RootCssClass("gridster-holder");

            LoadJavascriptModule("scripts/widgetModule.js", "run()");
        }
    }
}