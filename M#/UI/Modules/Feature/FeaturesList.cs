using MSharp;
using System;

namespace Modules
{
    public class FeaturesList : ListModule<Domain.Feature>
    {
        public FeaturesList()
        {
            HeaderText("Features").DataSource("Feature.All").IndexColumn();

            Search(GeneralSearch.ClientSideFilter).WatermarkText("Search...").NoLabel();

            CustomColumn().DisplayExpression("@item.GetFullPath()").LabelText("Feature");

            Column(f => f.Service);

            Column(i => i.ImplementationUrl).LabelText("Implementation");

            Column(f => f.UseIframe).AsText();

            CustomColumn().HeaderTemplate("Permissions")
                .DisplayExpression(@"@(item.Permissions.ToString("",""))<br/>
                                     @(item.NotPermissions.ToString("","").WithPrefix(""Not: ""))");
        }
    }
}