using MSharp;

namespace Modules
{
    public class SubFeatureView : ViewModule<Domain.Feature>
    {
        public SubFeatureView()
        {
            IsViewComponent().RootCssClass("feature-frame-view");
            SecurityChecks("User.Identity.IsAuthenticated");

            ViewModelProperty<string>("SubFeatureImplementationUrl");
            ViewModelProperty<string>("RedirectUrl")
                .Getter("Item.ToHubSubFeatureUrl(SubFeatureImplementationUrl)");

            OnBound("Load the page")
               .Code(@"JavaScript(JavascriptModule.Alias(""app/hub""),
                         $""go(\""{info.RedirectUrl}\"", {info.Item.UseIframe.ToString().ToLower()})"");");
        }
    }
}