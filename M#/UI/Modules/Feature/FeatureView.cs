using MSharp;

namespace Modules
{
    public class FeatureView : ViewModule<Domain.Feature>
    {
        public FeatureView()
        {
            //IsViewComponent().
            RootCssClass("feature-frame-view");
            SecurityChecks("User.Identity.IsAuthenticated");

            OnBound("Load the page")
               .Code(@"JavaScript(JavascriptModule.Alias(""app/hub""),
                         $""go(\""{info.Item.LoadUrl}\"", {info.Item.UseIframe.ToString().ToLower()})"");");

            LoadJavascriptModule("scripts/featuresMenu/featuresMenu.js",
                "c#:$\"show('{info.Item.ID}')\"")
                .Criteria("info.Item?.ImplementationUrl.HasValue() == true && info.Item?.UseIframe == true");
        }
    }
}