using MSharp;

namespace Modules
{
    public class UnauthorizedAccess : ViewModule<Domain.Feature>
    {
        public UnauthorizedAccess()
        {
            RequestParam("feature");

            RootCssClass("error unauthorized");
            CustomField().DisplayExpression(@"<h2>Access denied!</h2>
<p>You do not appear to have access to <b>@info.Item.Title</b>.</p>
<p>In case your session is expired, try <a href='/login'>logging in</a> again.");
        }
    }
}