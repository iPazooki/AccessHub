using Domain;
using MSharp;

namespace Modules
{
    public class ChildFeaturesList : ListModule<Domain.Feature>
    {
        public ChildFeaturesList()
        {
            RenderMode(ListRenderMode.List)
                .RootCssClass("feature-children")
                //.HeaderText("@Model.Parent.ToStringOrEmpty()")
                .DataSource("Feature.All.Where(x => x.Parent == info.Parent && x.Title != \"WIDGETS\")")
                .SourceCriteria("Context.Current.User().CanSee(item)");

            ViewModelProperty<Feature>("Parent").FromRequestParam("parent");

            Search(GeneralSearch.ClientSideFilter)
                .NoLabel()
                .WatermarkText("Search...")
                .RowCssClass("justify-content-center d-none d-lg-flex d-xl-flex");

            var button = LinkColumn(@"<i class=""fa-2x @item.GetIcon()"" aria-hidden=""true""></i>@item.GetTitle(Model.Parent) <small>@item.GetDescription()</small>")
                .CssClass("feature-button olive-instant-search-item badge-number")
                .Name("Title")
                .ExtraTagAttributes("id=\"@item.ID\" @(\"data-redirect='ajax'\".OnlyWhen(!item.UseIframe).Raw()) data-badgeurl=\"@item.GetBadgeUrl()\" data-service=\"@item.Service?.Name\"")
                .OnClick(x => x.Go("c#:item.LoadUrl"));

            Markup(button.Ref);
        }
    }
}
