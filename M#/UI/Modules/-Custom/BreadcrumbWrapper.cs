using MSharp;

namespace Modules
{
    public class BreadcrumbWrapper : GenericModule
    {
        public BreadcrumbWrapper()
        {
            IsViewComponent().IsInUse().WrapInForm(value: false).RootCssClass("no-print");

            Markup(
                @"<nav aria-label=""breadcrumb"">
                     <ol class=""breadcrumb"">                        
                     </ol>
                  </nav>");
        }
    }
}