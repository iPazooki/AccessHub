using MSharp;

namespace Modules
{
    public class Footer : GenericModule
    {
        public Footer()
        {
            IsInUse().IsViewComponent().RootCssClass("logout-box");

            var signout = Link("Sign out").OnClick(x => x.Go("/logout"));

            Markup($@"<hr/>
                    <div>Hi @User?.Identity.Name</div>
                    <div>{signout.Ref} </div>");
        }
    }
}