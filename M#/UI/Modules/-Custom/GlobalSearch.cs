using MSharp;

namespace Modules
{
    public class GlobalSearch : GenericModule
    {
        public GlobalSearch()
        {
            IsViewComponent().RootCssClass("no-print");
            Markup(@"<input type=""text"" name=""searcher"" 
              placeholder=""Search...""
              class=""form-control global-search""
              data-search-source=""@Model.GetSearchSources()"" />");
        }
    }
}