﻿using MSharp;

namespace Modules
{
    public class FeaturesTopMenuWrapper : GenericModule
    {
        public FeaturesTopMenuWrapper()
        {
            IsViewComponent().IsInUse().WrapInForm(value: false);

            ViewModelProperty<string>("Markup")
               .OnBound("info.Markup = (await AuthroziedFeatureInfo.RenderMenuJson()).ToString();");

            Markup("@info.Markup.Raw() <button type=\"button\" id=\"sidebarCollapse\" class=\"navbar-btn d-none d-lg-block\"><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i></button>    <ul class='features-sub-menu feature-top-menu d-none d-lg-flex'></ul>    <script id=\"sumMenu-template\" type=\"text/x-handlebars-template\">        {{#menus}}        <ul class=\"nav navbar-nav\">            {{#Children}}            <li class=\"feature-menu-item\" data-nodeid=\"{{ID}}\">                <i class=\"{{Icon}}\" aria-hidden=\"true\"></i>                <a href=\"{{LoadUrl}}\" data-service=\"\" {{#if UseIframe}} {{else}} data-redirect=\"ajax\" {{/if}}>{{Title}}</a>                <ul>                    {{#Children}}                    <li class=\"feature-menu-item\" data-nodeid=\"{{ID}}\">                        <i class=\"{{Icon}}\" aria-hidden=\"true\"></i>                        <a href=\"{{LoadUrl}}\" data-service=\"\" {{#if UseIframe}} {{else}} data-redirect=\"ajax\" {{/if}}>{{Title}}</a><ul>                    {{#Children}}                    <li class=\"feature-menu-item\" data-nodeid=\"{{ID}}\">                        <i class=\"fa {{Icon}}\" aria-hidden=\"true\"></i>                        <a href=\"{{LoadUrl}}\" data-service=\"\" {{#if UseIframe}} {{else}} data-redirect=\"ajax\" {{/if}}>{{Title}}</a>                    </li>                    {{/Children}}                </ul>                    </li>                    {{/Children}}                </ul>            </li>            {{/Children}}        </ul>        {{/menus}}    </script>").VisibleIf(CommonCriterion.IsUserLoggedIn);
        }
    }
}