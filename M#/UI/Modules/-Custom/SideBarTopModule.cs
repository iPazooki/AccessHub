using MSharp;

namespace Modules
{
    public class SideBarTopModule : GenericModule
    {
        public SideBarTopModule()
        {
            IsInUse().IsViewComponent();

            var logo = Image("Logo")
                .CssClass("logo")
                .ImageUrl("~/img/Logo.png")
                .MarkupTemplate("<div class=\"logo-wrapper\"><div>[#Button#]</div></div>")
                .OnClick(x => x.Go("~/"));

            var refe = Reference<GlobalSearch>();

            Markup($"{logo.Ref}" + "@(await Component.InvokeAsync<GlobalSearch>())");
        }
    }
}