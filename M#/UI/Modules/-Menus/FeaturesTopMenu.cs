﻿using System.Collections.Generic;
using Domain;
using MSharp;

namespace Modules
{
    public class FeaturesTopMenu : MenuModule
    {
        public FeaturesTopMenu()
        {
            IsViewComponent().IsInUse().WrapInForm(false).UlCssClass("features-sub-menu ");
            ViewModelProperty<Feature>("Parent");
            ViewModelProperty<Feature>("ViewingFeature");
            ViewModelProperty<IEnumerable<Feature>>("Items");

            OnPreBound("Set ViewingFeature")
                            .Code($"info.ViewingFeature = Website.FeatureContext.ViewingFeature;");

            OnPreBound("Set the items").Code("info.Items = info.Parent.Children;");
            OnPreBound("Include the parent if it has implementation")
                .Criteria("info.Parent.ImplementationUrl.HasValue()")
                .Code("info.Items = new[] { info.Parent }.Union(info.Items);");

            SpecialSelectedKeyRule("return info.Items.Reverse()" +
                ".FirstOrDefault(f => f.WithAllChildren().Contains(info.ViewingFeature))?.ID.ToString();");

            Item("c#:item.Title")
                .DataSource("Model.Items")
                .LinkCssClass("c#:\"badge-number\".OnlyWhen(item.BadgeUrl.HasValue())")
                .CustomAttributes("data-redirect='ajax' data-badgeurl=\"@item.BadgeUrl\" data-service=\"@item.Service?.Name\"")
                .Key("c#:item.ID")
                .CssClass("c#:\"feature-menu-item\" + \" feature-box\".OnlyWhen(Model.ViewingFeature.ImplementationUrl.IsEmpty() && item.Parent == Model.ViewingFeature)")
                .OnClick(x => x.Go("c#:item.LoadUrl"));

            Item("Child").Parent(Item("Parent"));
        }
    }
}