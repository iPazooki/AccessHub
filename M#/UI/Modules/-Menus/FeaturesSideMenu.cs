using System.Collections.Generic;
using Domain;
using MSharp;

namespace Modules
{
    public class FeaturesSideMenu : GenericModule
    {
        const int SIDE_MENU_PARENT_INDEX = 1;
        public FeaturesSideMenu()
        {
            RootCssClass("features-side-menu");
            AjaxRedirect().IsViewComponent().WrapInForm(false);

            ViewModelProperty<string>("Markup")
                .OnBound("info.Markup = (await AuthroziedFeatureInfo.RenderMenu(Website.FeatureContext.ViewingFeature)).ToString();");

            Markup("@info.Markup.Raw()");
        }
    }
}