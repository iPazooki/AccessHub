using MSharp;

namespace Modules
{
    public class ServiceView : ViewModule<Domain.Service>
    {
        public ServiceView()
        {
            RequestParam("service");
            SecurityChecks("User.Identity.IsAuthenticated");
            ViewModelProperty<string>("Url");
            ViewModelProperty<string>("ActualRelativeUrl")
                .Getter("Url.ToLower().TrimStart($\"/{ Item.Name.ToLower()}/\")");

            ViewModelProperty<string>("DestinationUrl")
                .Getter("Item.GetHubImplementationUrl(ActualRelativeUrl)");

            OnBound("Load the page")
               .Criteria("info.Url.HasValue()")
               .Code(@"JavaScript(JavascriptModule.Alias(""app/hub""),
                         $""go(\""{info.DestinationUrl}\"", {info.Item.UseIframe.ToString().ToLower()})"");");
        }
    }
}