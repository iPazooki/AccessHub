using MSharp;

namespace Modules.Service
{
    public class ServicesList : ListModule<Domain.Service>
    {
        public ServicesList()
        {
            Using("AppContentService");
            HeaderText("Services");
            Header("@Html.AppContent(\"Hub.Services.Intro\")");
            DataSource("Service.All");

            Search(GeneralSearch.AllFields).Label("Find:");

            Column(x => x.Name);
            LinkColumn("c#:item.BaseUrl").HeaderText("Base url")
                .OnClick(x => x.Go("c#:item.BaseUrl").Target(OpenIn.NewBrowserWindow));
        }
    }
}