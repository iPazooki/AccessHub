﻿using MSharp;

namespace Modules
{
    public class SingleSignOn : ListModule<Domain.Service>
    {
        public SingleSignOn()
        {
            RenderMode(ListRenderMode.List);
            DataSource("Service.All.Where(x => x.InjectSingleSignon)");
            HeaderText("Logging in to other apps...");
            Markup(@"<div style='display:block; float:left; margin:15px; width:150px; height:150px;border:1px solid gray;'>
                         @item
                         <iframe style='width:150px; height:150px; border: none'
                                 src='@listItem.ServiceUrl?ticket=@info.Ticket'>
                         </iframe>
                     </div>");
            Header("@Model.Errors.Raw()");

            ViewModelProperty<string>("ServiceUrl")
                .Getter(@"Item.GetAbsoluteImplementationUrl(""@Services/SSO.ashx"")")
                .PerListItem();

            ViewModelProperty<string>("Token").OnBound("info.Token = CreateToken(info.Ticket);");

            OnPostBound("Prepare apps").Code("await PrepareApps(info);");

            OnJavascript("Wait time").Code("function waitTime() { if (screen.width < 500) return 5000; else return 2000; }");
            OnJavascript("Redirect").Code("setTimeout(function() { window.location.href='/' }, waitTime())");
        }
    }
}