using MSharp;
using Olive;
using System.Linq;

namespace Modules
{
    public class ManualLogin : FormModule<PeopleService.UserInfo>
    {
        public ManualLogin()
        {
            IsViewComponent()
                .RootCssClass("manual-login")
                .VisibleIf("info.AllowManual")
                .HeaderText("Bypass auth<br/>(dev time only)<br/><br/>");

            Field(x => x.DisplayName).NoLabel();
            Field(x => x.Email).NoLabel();
            CustomField().NoLabel()
                .PropertyName("RoleNames")
                .Control(ControlType.Textbox)
                .ExtraControlAttributes("rows='10'");

            AutoSet(x => x.DisplayName).Value("\"Jack Smith\"");
            AutoSet(x => x.Email).Value("\"jack.smith@geeks.ltd\"");

            ViewModelProperty<bool>("AllowManual")
                .Getter("Config.Get(\"Authentication:AllowManual\", defaultValue: false)");

            OnPreBinding("Set default roles")
                .Criteria("Request.IsGet()")
                .Code($"info.RoleNames = @\"Employee,{GetAllRoles()}\";");

            Button("Simulate log in").OnClick(x =>
            {
                x.RunInTransaction(false);
                x.CSharp("await info.CopyDataTo(info.Item);");
                x.CSharp("info.Item.Roles = info.RoleNames.Split(',').Trim().ToArray();");
                x.CSharp("await info.Item.LogOn();");
                x.Go("c#:Request.Param(\"returnUrl\")");
            });
        }

        string GetAllRoles()
        {
            return "Dev,QA,BA,PM,AM,Director,Designer,IT,Reception,PA,Sales".Split(',')
                .SelectMany(r => ",Junior,Senior,Lead,Head".Split(',').Select(l => l + r))
                .ToString(",");
        }
    }
}