using MSharp;

namespace Modules
{
    public class LoginForm : FormModule<Domain.User>
    {
        public LoginForm()
        {
            UseAntiForgeryToken(value: false)
                .Using(new[] { "Microsoft.AspNetCore.Authentication", "Olive.Security" })
                .RootCssClass("social-media-login");

            ViewModelProperty<string>("ErrorMessage");

            var googleLogin = Button("Login by Google")
                .Name("LoginByGoogle")
                .ExtraTagAttributes("formmethod='post'")
                .CssClass("btn-social btn-google btn btn-primary")
                .Icon(FA5.UserLock)
                .OnClick(x =>
                 {
                     x.RunInTransaction(false);
                     x.CSharp("await OAuth.Instance.LoginBy(\"Google\");");
                 });

            Header($@"<div class='google-login-wrapper'>
                         <h2>Geeks Access Hub</h2>{googleLogin.Ref}
                      </div>");

            OnBound("Clear cookies").Code(@"  
            var alreadyDead = new Microsoft.AspNetCore.Http.CookieOptions
            {
                Expires = LocalTime.Today.AddDays(-1)
            };

            foreach (var c in Request.Cookies)
                Response.Cookies.Append(c.Key, string.Empty, alreadyDead);");

            CustomField().NoLabel().ControlMarkup(@"@Model.ErrorMessage.Raw()");
        }
    }
}