﻿using MSharp;

public class SSOPage : RootPage
{
    public SSOPage()
    {
        this.NoGuestAccess();
        Add<Modules.SingleSignOn>();
    }
}

