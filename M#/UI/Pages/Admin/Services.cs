﻿using Modules.Service;
using MSharp;

namespace Admin.Services
{
    class ServicesPage : SubPage<AdminPage>
    {
        public ServicesPage()
        {
            Add<ServicesList>();
        }
    }
}