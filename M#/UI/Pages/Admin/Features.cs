﻿using Modules;
using MSharp;

namespace Admin.Features
{
    class FeaturesPage : SubPage<AdminPage>
    {
        public FeaturesPage()
        {
            this.NoGuestAccess();
            Add<FeaturesList>();
        }
    }
}