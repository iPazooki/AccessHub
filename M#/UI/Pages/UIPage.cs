using MSharp;

public class UIPage : RootPage
{
    public UIPage()
    {
        Route("/UI \n [#EMPTY#]");
        BrowserTitle("Geeks Access Hub");

        Set(PageSettings.LeftMenu, "FeaturesSideMenu");

        this.NoGuestAccess();

        Add<Modules.FeatureView>();
    }
}