﻿namespace MSharp
{
    static class Extensions
    {
        public static TPage NoGuestAccess<TPage>(this TPage @this)
             where TPage : ApplicationPage
        {
            @this.OnStart(x => x.If(CommonCriterion.IsUserGuest_notLogged_in)
            .Go<LoginPage>()
            .SendReturnUrl()
            .RunServerSide());

            return @this;
        }
    }
}
