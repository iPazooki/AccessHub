using MSharp;

namespace UI.Board
{
    public class PersonBoardPage : SubPage<UIPage>
    {
        public PersonBoardPage()
        {
            Route("person/{featureId}");
            Roles(AppRole.Employee);
            Add<Modules.BoardView>().OnBinding("info.Item = Board.Parse(\"Person\");");
        }
    }
}