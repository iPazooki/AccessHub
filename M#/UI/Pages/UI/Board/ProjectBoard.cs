using MSharp;

namespace UI.Board
{
    public class ProjectBoardPage : SubPage<UIPage>
    {
        public ProjectBoardPage()
        {
            Route("project/{featureId}");
            Roles(AppRole.Employee);
            Add<Modules.BoardView>().OnBinding("info.Item = Board.Parse(\"Project\");");
        }
    }
}

