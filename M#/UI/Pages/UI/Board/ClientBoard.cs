using MSharp;

namespace UI.Board
{
    public class ClientBoardPage : SubPage<UIPage>
    {
        public ClientBoardPage()
        {
            Route("client/{featureId}");
            Roles(AppRole.Employee);
            Add<Modules.BoardView>().OnBinding("info.Item = Board.Parse(\"Client\");");
        }
    }
}

