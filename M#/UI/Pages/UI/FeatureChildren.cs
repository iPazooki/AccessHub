﻿using Modules;
using MSharp;
using Olive;

namespace Admin.Features
{
    class FeaturechildrenPage : SubPage<UIPage>
    {
        public FeaturechildrenPage()
        {
            this.NoGuestAccess();
            Route(new[] { "/feature/children/{parent}", "/root", "/under" }.ToLinesString());
            Set(PageSettings.LeftMenu, "FeaturesSideMenu");
            Add<ChildFeaturesList>();
        }
    }
}