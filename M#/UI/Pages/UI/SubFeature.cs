using MSharp;

namespace UI
{
    public class SubFeaturePage : SubPage<UIPage>
    {
        public SubFeaturePage()
        {
            this.NoGuestAccess();
            Route("UI/SubFeature/{item}");
            Add<Modules.SubFeatureView>();
        }
    }
}