using MSharp;

namespace UI
{
    public class UnauthorizedPage : SubPage<UIPage>
    {
        public UnauthorizedPage()
        {
            Route("UI/Unauthorized/{feature}");
            Add<Modules.UnauthorizedAccess>();
        }
    }
}