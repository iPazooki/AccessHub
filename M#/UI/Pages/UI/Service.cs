using MSharp;

namespace UI
{
    public class ServicePage : SubPage<UIPage>
    {
        public ServicePage()
        {
            this.NoGuestAccess();
            Route("/UI/Service/{service}");
            Add<Modules.ServiceView>();
        }
    }
}

