using MSharp;

namespace UI
{
    public class FeaturePage : SubPage<UIPage>
    {
        public FeaturePage()
        {
            this.NoGuestAccess();
            Route("UI/Feature/{item}");
            Add<Modules.FeatureView>();
            BrowserTitle("c#:info.Item.GetFullPath()");
        }
    }
}

