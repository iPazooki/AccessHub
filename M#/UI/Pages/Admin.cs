using MSharp;

public class AdminPage : RootPage
{
    public AdminPage()
    {
        Roles(AppRole.Director);
        Set(PageSettings.LeftMenu, "FeaturesSideMenu");
        OnStart(x => x.Go<Admin.Features.FeaturesPage>().RunServerSide());
    }
}
