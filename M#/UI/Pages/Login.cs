using MSharp;

public class LoginPage : RootPage
{
    public LoginPage()
    {
        OnStart(x => x.If("Request.Param(\"returnUrl\").IsEmpty()")
        .Go<LoginPage>().Send("ReturnUrl", "\"/\""));

        RootCssClass("login-page");
        Add<Modules.ManualLogin>();
        Add<Modules.LoginForm>();
    }
}