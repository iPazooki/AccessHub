﻿using MSharp;

namespace Domain
{
    public class Permission : EntityType
    {
        public Permission()
        {
            String("Name").Mandatory().Unique();
        }
    }
}
