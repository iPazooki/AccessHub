using MSharp;

namespace Domain
{
    public class Service : EntityType
    {
        public Service()
        {
            InstanceAccessors();
            DatabaseMode(DatabaseOption.Transient);
            String("Name").Unique();
            String("BaseUrl");
            Bool("UseIframe").Mandatory();
            Bool("Inject single signon").Mandatory();
        }
    }
}