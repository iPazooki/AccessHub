using MSharp;

namespace Domain
{
    public class Feature : EntityType
    {
        public Feature()
        {
            IsHirarchy().Sortable()
            .ToStringExpression("this.WithAllParents().Select(s=>s.Title).ToString(\" > \")");
            DatabaseMode(DatabaseOption.Transient);

            Bool("Use iframe").Mandatory();
            DefaultSort = Int("Order").Mandatory();
            String("Title").Mandatory();
            String("Description");
            String("Ref").Unique();
            String("Load url").Mandatory();
            String("Implementation url");
            String("Icon");

            String("Badge url");
            String("Badge optional for");

            Associate<Feature>("Parent");
            Associate<Service>("Service").Mandatory();
            Associate<Feature>("GrandParent").CalculatedFrom("Parent?.Parent");

            Associate<Feature>("Children").MaxCardinality(null);
            Associate<Permission>("Permissions").MaxCardinality(null);
            Associate<Permission>("Not permissions").MaxCardinality(null);
        }
    }
}
