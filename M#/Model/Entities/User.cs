using MSharp;

namespace Domain
{
    public class User : EntityType
    {
        public User()
        {
            String("Email").Accepts(TextPattern.EmailAddress);

            // To ensure the meta-data is added for use in the UI:
            Associate<PeopleService.UserInfo>("Info");
        }
    }
}