using MSharp;

namespace Domain
{
    public class Widget : EntityType
    {
        public Widget()
        {
            DatabaseMode(DatabaseOption.Transient);
            SortableByOrder();

            String("Title").Mandatory();
            String("Colour").Mandatory();
            Associate<Feature>("Feature").Mandatory();
            Associate<Feature>("Settings");
            Associate<Board>("Board").Mandatory();
        }
    }
}