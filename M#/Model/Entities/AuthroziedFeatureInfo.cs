using MSharp;

namespace Domain
{
    public class AuthroziedFeatureInfo : EntityType
    {
        public AuthroziedFeatureInfo()
        {
            DatabaseMode(DatabaseOption.Transient);

            Associate<Feature>("Feature");
            Bool("IsDisabled").Mandatory();
        }
    }
}