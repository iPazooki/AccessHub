using MSharp;

namespace Domain
{
    public class Board : EntityType
    {
        public Board()
        {
            DatabaseMode(DatabaseOption.Transient);
            String("Name").Mandatory();

            InverseAssociate<Widget>("Widgets", "Board");
        }
    }
}