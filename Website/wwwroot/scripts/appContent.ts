﻿﻿import Waiting from "olive/components/waiting";
import Service from "app/model/service";
import FormAction from "olive/mvc/formAction";
import Modal from "olive/components/modal";
import AjaxRedirect from "olive/mvc/ajaxRedirect";

export default class AppContent {
    input: JQuery;

    public static enableContentBlock(selector: JQuery) {
        selector.each((i, e) => new AppContent($(e)).enableContentBlock());
    }

    public static enableHelp(selector: JQuery) {
        selector.each((i, e) => new AppContent($(e)).enableHelp());
    }

    constructor(targetInput: any) {
        this.input = targetInput;
    }

    enableContentBlock(): void {

        let keyParam = this.input.attr("key");

        Waiting.show(true, false);

        let serviceUrl = Service.fromName("AppContent");

        let url = serviceUrl.BaseUrl + "/api/getContent/" + keyParam;

        $.ajax({
            url: url,
            type: 'GET',
            xhrFields: { withCredentials: true },
            success: (response: any) => {
                this.input.replaceWith(response);
            },
            error: (response) => {
                console.log(response);
            },
            complete: (response) => {
                Waiting.hide();
                AjaxRedirect.enableRedirect($("a[data-redirect=ajax]"));
            }
        });
    }

    enableHelp(): void {

        let keyParam = this.input.attr("key");

        Waiting.show(true, false);

        let serviceUrl = Service.fromName("AppContent");

        let url = serviceUrl.BaseUrl + "/api/getContent/" + keyParam + "/true";

        $.ajax({
            url: url,
            type: 'GET',
            xhrFields: { withCredentials: true },
            success: (response: any) => {

                this.input.replaceWith(response);

                $(() => {
                    $('[data-toggle="popover"]').popover({ trigger: 'focus' });
                });
            },
            error: (response) => {
                console.log(response);
            },
            complete: (response) => {
                Waiting.hide();
                AjaxRedirect.enableRedirect($("a[data-redirect=ajax]"));
            }
        });
    }
}
