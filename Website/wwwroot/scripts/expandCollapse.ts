﻿declare var requirejs: any;

export default class ExpandCollapse {
    input: JQuery;

    constructor(targetInput: any) {
        this.input = targetInput;
    }

    public static enableExpandCollapse(selector: JQuery) {
        selector.each((i, e) => new ExpandCollapse($(e)).enableExpandCollapse());
    }

    enableExpandCollapse(): void {
        requirejs(["js-cookie"], x => this.doEnableExpandCollapse(x));
    }

    doEnableExpandCollapse(Cookies: any) {
        //check cookie for the initial menu state.    
        let leftMenu = Cookies.get("hub_left_menu");

        if (leftMenu && leftMenu == "collapsed") {
            $(".side-bar").addClass("collapsed");
            this.changeIcon(this.input.select().children("i"));
            this.input.addClass("collapse");
        }
        else {
            this.input.removeClass("collapse");
        }

        this.input.click(() => {

            $(".side-bar").toggleClass("collapsed");

            this.input.toggleClass("collapse");

            if ($(".side-bar").hasClass("collapsed")) {
                Cookies.set("hub_left_menu", "collapsed", { expires: 7 });
            }
            else {
                Cookies.set("hub_left_menu", "");
            }

            this.changeIcon(this.input.select().children("i"));

            this.syncHubFrame();

        });
    }

    changeIcon(childIcon: JQuery) {

        if (childIcon.hasClass("fa-chevron-left")) {
            childIcon.removeClass("fa-chevron-left").addClass("fa-chevron-right");
        }
        else {
            childIcon.removeClass("fa-chevron-right").addClass("fa-chevron-left");
        }

        this.syncHubFrame();
    }

    syncHubFrame() {
        this.notifyHub('setViewFrameHeight', Math.round($("service").height()));
    }

    notifyHub(command: string, arg) {
        let paramW = { command: command, arg: arg };
        window.parent.postMessage(JSON.stringify(paramW), "*");
    }
}