﻿
export default class CheckboxList {
    input: JQuery;

    constructor(targetInput: any) {
        this.input = targetInput;
    }

    public static enableCheckboxList(selector: JQuery) {
        selector.each((i, e) => new CheckboxList($(e)).enableCheckboxList());
    }

    enableCheckboxList(): void {
        this.input.removeClass().addClass("cb");
        this.input.next().removeClass().addClass("cb__label");
    }
}