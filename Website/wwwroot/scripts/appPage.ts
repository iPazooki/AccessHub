﻿import OlivePage from 'olive/olivePage';
import FeaturesMenu from 'app/featuresMenu/featuresMenu';
import AppContent from 'app/appContent';
import BadgeNumber from 'app/badgeNumber';
import CheckboxList from 'app/checkboxList';
import ToggleCheckbox from 'app/toggleCheckbox';
import WidgetModule from 'app/widgetModule';
import ExpandCollapse from 'app/expandCollapse';
import BreadcrumbMenu from 'app/featuresMenu/breadcrumbMenu';

export default class AppPage extends OlivePage {

    // Here you can override any of the base standard functions.
    // e.g: To use a different AutoComplete library, simply override handleAutoComplete(input).

    constructor() {
        super();

        (<any>window).requirejs(["../scripts/hub"], hub => hub.default.initialize());
        BadgeNumber.enableBadgeNumber($("a[data-badgeurl]"));

        //every 5 min badge numbers should be updated
        window.setInterval(() => { BadgeNumber.enableBadgeNumber($("a[data-badgeurl]")); }, 5 * 60 * 1000);

        ExpandCollapse.enableExpandCollapse($("#sidebarCollapse"));
        $(() => {
            // set global search focused
            $("input.global-search").each((i, e: any) => e.focus());
        });

        $("#iFrameHolder").hide(); //hide iFrame initially
    }

    revive() {
        super.initialize();
    }

    initialize() {
        super.initialize();
        FeaturesMenu.bindItemListClick();
        BreadcrumbMenu.bindItemListClick();
        AppContent.enableContentBlock($("AppContent"));
        AppContent.enableHelp($("Help"));
        CheckboxList.enableCheckboxList($("input[class='form-check-input']"));
        ToggleCheckbox.enableToggleCheckbox($("input[class='form-check']"));
        WidgetModule.enableWidget($("Widget"));

        // This function is called upon every Ajax update as well as the initial page load.
        // Any custom initiation goes here.
    }
}

window["page"] = new AppPage();