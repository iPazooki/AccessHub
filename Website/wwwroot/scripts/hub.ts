﻿/// <amd-dependency path='olive/olivePage' />

import Url from 'olive/components/url';
import Form from 'olive/components/form';
import Config from 'olive/config';
import FeaturesMenu from 'app/featuresMenu/featuresMenu';
import NavigationManager from 'app/featuresMenu/featuresMenuNavigationManager';
import AjaxRedirect from 'olive/mvc/ajaxRedirect';
import CrossDomainEvent from 'olive/components/crossDomainEvent';
import Service from 'app/model/service';
import BreadcrumbMenu from 'app/featuresMenu/breadcrumbMenu';

export default class Hub {

    public static initialize() {
        Service.registerServices();
        AjaxRedirect.onRedirected = (title, url) => Service.onNavigated(url, title);
        AjaxRedirect.onRedirectionFailed = (url, response) => NavigationManager.navigationFailed(url, response);
        FeaturesMenu.enableFeaturesTreeView();
        BreadcrumbMenu.enableBreadcrumb();
        window["resolveServiceUrl"] = Url.effectiveUrlProvider = this.getEffectiveUrl;
        Form.currentRequestUrlProvider = this.getCurrentRequestUrl;

        CrossDomainEvent.handle("setViewFrameHeight", h => this.setViewFrameHeight(h));
        CrossDomainEvent.handle("setServiceUrl", u => Service.onNavigated(u.url, u.title));
    }

    static getCurrentRequestUrl(): string {
        let result = window.location.pathAndQuery().trimStart("/");

        var slash = result.indexOf('/');
        if (slash > 0) result = result.substring(slash);

        return result;
    }

    static setViewFrameHeight(height) {

        if (height <= 0) return;

        height = Math.max($(".side-bar").height() - 400, height);

        var currentFrameHeight = $("iframe.view-frame").height();

        if (currentFrameHeight < height) this.setiFrameHeight(height);
        else {
            // Frame is larger. But is it too large?
            if (currentFrameHeight > height + 150) this.setiFrameHeight(height);
        }
    }

    static setiFrameHeight(height: number) {
        $("iframe.view-frame").css("cssText", "height: " + (height + 80) + "px !important;");
    }

    public static getEffectiveUrl(url: string, trigger: JQuery = null): string {
        $("#iFrameHolder").hide(); //hide any opened iFrame content after ajax call.

        if (Url.isAbsolute(url)) return url;

        let serviceName: string;
        let serviceContainer = trigger ? trigger.closest("service[of]") : $("service[of]");

        if (serviceContainer.length === 0) serviceContainer = $("service[of]")

        if (serviceContainer.length === 0)
            throw new Error("<service of='...' /> is not found on the page.");

        serviceName = serviceContainer.attr("of");

        url = url.trimStart("/");
        // Explicitly specified on the link?
        if (url.startsWith("[") && url.contains("]")) {
            serviceName = url.substring(1, url.indexOf("]"));
            url = url.substring(serviceName.length + 2);
            serviceContainer.attr("of", serviceName);
        }

        //All urls starting with "under" are from HUB service.
        if (url.startsWith("under")) {
            serviceName = "hub";
        }

        var baseUrl = Service.fromName(serviceName).BaseUrl;
        if (!baseUrl.startsWith("http"))
            baseUrl = baseUrl.withPrefix("http://");

        return Url.makeAbsolute(baseUrl, url);
    }

    public static go(url: string, iframe: boolean) {
        if (iframe) {
            url = this.getEffectiveUrl(url);
            $("iframe.view-frame").attr("src", url);
            $(".feature-frame-view").show();
            $("main").hide();
        }
        else AjaxRedirect.go(url);
    }
}

