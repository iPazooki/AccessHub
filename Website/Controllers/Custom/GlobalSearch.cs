﻿using Domain;
using Olive;
using Olive.Mvc;
using System.Linq;

namespace ViewModel
{
    partial class GlobalSearch
    {
        public string GetSearchSources()
        {
            var olive = "Hub,Tasks".Split(',')
                .Select(s => Service.FindByName(s))
                .Select(s => s.GetAbsoluteImplementationUrl("/api/global-search"));

            var webForms = "Accounting,Dashboard,HR,CRM,CaseStudies,Training".Split(',')
                .Select(s => Service.FindByName(s))
                .Select(s => s.GetAbsoluteImplementationUrl("/global-search.axd"));

            return olive.Concat(webForms).ToString(";");
        }
    }
}