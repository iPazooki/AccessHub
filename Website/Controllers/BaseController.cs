﻿using Microsoft.AspNetCore.Mvc;
using Olive;

namespace Controllers
{
    public class BaseController : Olive.Mvc.Controller
    {
        public BaseController()
        {
            ApiClient.FallBack.Handle(arg => Notify(arg.FriendlyMessage, false));
        }

        [NonAction]
        public new ActionResult Unauthorized() => Redirect("/login");
    }
}

namespace ViewComponents
{
    public abstract class ViewComponent : Olive.Mvc.ViewComponent { }
}