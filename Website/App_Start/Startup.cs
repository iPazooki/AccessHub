﻿namespace Website
{
    using Domain;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Olive;
    using Olive.Entities.Data;
    using Olive.Hangfire;
    using Olive.Mvc.Testing;
    using Olive.Security;
    using System;
    using System.Globalization;
    using System.Threading.Tasks;

    public class Startup : Olive.Mvc.Startup
    {
        public Startup(IHostingEnvironment env, IConfiguration config)
            : base(env, config)
        {
            if (env.IsProduction()) config.LoadAwsIdentity();
        }

        protected override CultureInfo GetRequestCulture() => new CultureInfo("en-GB");

        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            services.AddScheduledTasks();

            if (Environment.IsDevelopment())
                services.AddDevCommands(x => x
                .AddTempDatabase<SqlServerManager, ReferenceData>()
                 .AddClearApiCache());
        }

        public override void Configure(IApplicationBuilder app)
        {
            base.Configure(app);

            app.UseGlobalSearch<GlobalSearchSource>();

            Console.Title = Microservice.Me.Name;

            if (Config.Get<bool>("Automated.Tasks:Enabled"))
                app.UseScheduledTasks(TaskManager.Run);

            Feature.DataProvider.Register();
            Service.DataProvider.Register();
            AppContentService.HubApi.DefaultConfig(config => config.Cache(CachePolicy.CacheOrFreshOrNull));
        }

        protected override void ConfigureSecurity(IApplicationBuilder app)
        {
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyHeader());
            base.ConfigureSecurity(app);
        }

        protected override void ConfigureRequestHandlers(IApplicationBuilder app)
        {
            app.UseStructureDeserializer();

            app.Use(async (r, next) =>
            {
                if (r.Request.Path.Value == "/" && r.Request.IsSmartPhone())
                    r.Response.Redirect("/root");
                else await next();
            });

            app.UseFeaturesViewPageMiddleware();
            base.ConfigureRequestHandlers(app);
        }

        protected override void ConfigureAuthentication(AuthenticationBuilder auth)
        {
            base.ConfigureAuthentication(auth);

            auth.AddGoogle(config =>
            {
                config.ClientId = Config.Get("Authentication:Google:ClientId");
                config.ClientSecret = Config.Get("Authentication:Google:ClientSecret");

                if (Environment.IsDevelopment())
                {
                    var key = Config.Get("Authentication:CookieDataProtectorKey");
                    config.DataProtectionProvider = new SymmetricKeyDataProtector("AuthCookies", key);
                }
                else
                {
                    config.DataProtectionProvider = new Olive.Security.Aws.KmsDataProtectionProvider();
                }
            });
        }

        protected override void ConfigureAuthCookie(CookieAuthenticationOptions options)
        {
            base.ConfigureAuthCookie(options);

            if (Environment.IsProduction())
            {
                options.Cookie.Domain = Config.Get("Authentication:Cookie:Domain");
                options.DataProtectionProvider = new Olive.Security.Aws.KmsDataProtectionProvider();
            }
        }
    }
}