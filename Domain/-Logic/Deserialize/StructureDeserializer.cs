﻿using Olive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Domain
{
    public class StructureDeserializer
    {
        static AsyncLock ServiceLock = new AsyncLock();
        static AsyncLock FeaturesLock = new AsyncLock();
        static AsyncLock BoardsLock = new AsyncLock();

        public static Task Load() => Task.WhenAll(LoadServices(), LoadFeatures(), LoadBoards());

        public static Task ReLoad()
        {
            Service.All = null;
            Feature.All = null;
            Board.All = null;
            return Load();
        }

        static Task LoadServices()
        {
            return LockedRun(ServiceLock, () => Service.All == null, () =>
               {
                   var environment = Context.Current.Environment().EnvironmentName.ToLower();

                   Service.All = ReadXml("Services.xml").Select(x => new Service
                   {
                       Name = x.GetCleanName(),
                       UseIframe = x.GetValue<bool?>("@iframe") ?? false,
                       BaseUrl = x.GetValue<string>("@" + environment) ?? x.GetValue<string>("@url"),
                       InjectSingleSignon = x.GetValue<bool?>("@sso") ?? false

                   }).ToList();
               });
        }

        static async Task LockedRun(AsyncLock @lock, Func<bool> condition, Action action)
        {
            if (condition() == false) return;

            using (await @lock.Lock())
            {
                if (condition() == false) return;

                action();
            }
        }

        static IEnumerable<XElement> ReadXml(string file)
        {
            return AppDomain.CurrentDomain.WebsiteRoot().GetFile(file)
                .ReadAllText().To<XDocument>().Root.Elements();
        }

        static FeatureDefinition[] GetFeatureDefinitions()
        {
            var root = new FeatureDefinition(null, new XElement("ROOT"));

            var files = AppDomain.CurrentDomain.WebsiteRoot().GetFiles("Features*.xml");

            return files
                     .Select(x => ReadXml(x.Name))
                     .SelectMany(x => x)
                     .Select(x => new FeatureDefinition(root, x))
                     .ToArray();
        }

        static Task LoadFeatures()
        {
            return LockedRun(FeaturesLock, () => Feature.All == null, () =>
               {
                   Feature.All = GetFeatureDefinitions()
                   .SelectMany(x => x.GetAllFeatures())
                   .ExceptNull()
                   .ToList();

                   foreach (var item in Feature.All)
                       item.Children = Feature.All.Where(x => x.Parent == item);
               });
        }

        static Task LoadBoards()
        {
            return LockedRun(BoardsLock, () => Board.All == null, () =>
                {
                    if (Board.All != null) return;
                    Board.All = ReadXml("Boards.xml").Select(x => new Board(x)).ToList();
                });
        }
    }
}